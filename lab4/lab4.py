#!/usr/bin/env python
# coding: utf-8

# In[190]:


from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import precision_recall_fscore_support
import numpy as np
import sys


# In[191]:


datasets = ["amazon_cells_labelled.txt","imdb_labelled.txt","yelp_labelled.txt"]

#derive_sets() is meant to obtain datasets from which to train or test models with
def derive_sets(files):
    count = 0
    #for each of the files provided in the list assigned to the 'datasets' variable,
    #I obtain sentences from them for both a training text file, named 'trainingData.txt', and a testing text file named 'testingData.txt'
    
    #'trainingData.txt' is meant to provide data that I can use to train both the Naive Bayes and the Logistic Regression classifiers.
    #'testingData.txt' is meant to provide data that I can use to test the performance of both the Naive Bayes and the Logistic Regression classifiers. 
    for i in files:
        test = open("testingData.txt","w")
        train = open("trainingData.txt","w")
        currentfile = open(i,"r")
        for line in currentfile:
            if count == 0:
                #write to the training dataset file
                train.write(line)
                count = 1
            else:
                #write to the testing dataset file
                test.write(line)
                count = 0
        currentfile.close()
        train.close()
        test.close()
    return

derive_sets(datasets)


# In[192]:


#the write_results() function writes the results of the classifier to a file
#the write_results() function checks to see whether the results obtained was generated from either a Logistic Regression classifier
#or a Naive Bayes classifier and then it proceeds to check whether the results obtained were from either a normalized or unnormalized training
#after performing all of these checks, it then writes the results to the correct text file.
def write_results(results_array,classifier,version):
    if classifier == "lr":
        if version == "n":
            results = open("results-lr-n.txt","w")
        if version == "u":
            results = open("results-lr-u.txt","w")
    elif classifier == "nb":
        if version == "n":
            results = open("results-nb-n.txt","w")
        if version == "u":
            results = open("results-nb-u.txt","w")
    for i in results_array:
        results.write(i)
        results.write('\n')
    results.close()


# In[193]:


#train_and_testLR() trains a logistic regression model on the data given and calculates the accuracy of the logistic regression classifier
def train_and_testLR(features_array, features_test, classes, classifier,version):
    #the train_test_split() function is responsible for splitting a matrix of features into the matrix of features to be used for either training or testing. 
    X_trainset, X_testset,Y_trainset, Y_testset = train_test_split(features_array,classes,train_size = 0.80,random_state = 1234)
    #the fit() funtion is responsible for training the logistic regression classifier on the matrix of features extracted for training.
    lr = LogisticRegression().fit(X_trainset,Y_trainset)
    #the predict() function is responsble for predicting the class values of the test file given
    predictionValue = lr.predict(features_test)
    #the write_results() function is responsible for writing the results of the prediction into its appropraite file.
    write_results(predictionValue,classifier,version)
    #the y_true variable contains the gold labels for the 'test_sentences' test file provided.
    y_true = np.array(['1','0','1','0','0','0','0','1','0','1','1','1','1','1','0','0','0','1','1','0','0','1','0','0','1'])
    #the precision_recall_fscore_support() function is responsible for generating the precision, recall and fmeasure score of the classifier.
    k = precision_recall_fscore_support(y_true,predictionValue,average = 'micro')
    print("Precision value: ",k[0])
    print("Recall value: ",k[1])
    print("Fmeasure value: ",k[2])
    


# In[194]:


#train_and_testNB() trains a naive bayes model on the data given and calculates the accuracy of the naive bayes classifier
def train_and_testNB(features_array,features_test, classes,classifier,version):
    #the train_test_split() function is responsible for splitting a matrix of features into the matrix of features to be used for either training or testing.
    X_trainset, X_testset,Y_trainset, Y_testset = train_test_split(features_array,classes,train_size = 0.80,random_state = 1234)
    #the fit() funtion is responsible for training the naive bayes classifier on the matrix of features extracted for training.
    nb = MultinomialNB().fit(X_trainset,Y_trainset)
    #the predict() function is responsble for predicting the class values of the test file given
    predictionValue = nb.predict(features_test)
    #the write_results() function is responsible for writing the results of the prediction into its appropraite file.
    write_results(predictionValue,classifier,version)
    #the y_true variable contains the gold labels for the 'test_sentences' test file provided.
    y_true = np.array(['1','0','1','0','0','0','0','1','0','1','1','1','1','1','0','0','0','1','1','0','0','1','0','0','1'])
    #the precision_recall_fscore_support() function is responsible for generating the precision, recall and fmeasure score of the classifier.
    k = precision_recall_fscore_support(y_true,predictionValue,average = 'micro')
    print("Precision value: ",k[0])
    print("Recall value: ",k[1])
    print("Fmeasure value: ",k[2])
    
    


# In[195]:


#assemble() reconstructs the sentences read from the file without their classes
def assemble(s):
    sentence = ""
    for i in s:
        sentence = sentence +" "+ i
    return sentence


# In[196]:


#transformer() is used to scale down the impact of tokens that occur very frequently in a given corpus and that are hence
#empirically less informative than features that occur in a small fraction of the training corpus.
def transformer(features):
    t_transform = TfidfTransformer()
    transform_features = t_transform.fit_transform(features)
    return (transform_features)


# In[197]:


#vectorizer() extracts features from the data being used and vectorizes these features.
def vectorizer(s,test):
    vectorizer_object = CountVectorizer()
    extracted_features = vectorizer_object.fit_transform(s)
    test_features = vectorizer_object.transform(test)
    transformed_extracted_features = transformer(extracted_features)
    return[transformed_extracted_features,test_features]


# In[198]:


#normalize() is meant to normalize the information in the training and test files.
def normalize(filename,classifier_type,version):
    symbols = ["?","/",".",",",":",";","}","{","[","]","-","!"]
    prediction_values = []
    sentence_values = []
    test_sentences = []
    file_info = open("testingData.txt","r")
    test_file = open(filename,"r")
    for line in file_info:
        lowercase_line = line.lower()
        for i in symbols:
            if(i in lowercase_line):
                lowercase_line = lowercase_line.replace(i,"")
        splitted_line = lowercase_line.split()
        if splitted_line[len(splitted_line) - 1] == '0':
            prediction_values.append('0')
        if splitted_line[len(splitted_line) - 1] == '1':
            prediction_values.append('1')
        del splitted_line[len(splitted_line) - 1]
        sentence = assemble(splitted_line)
        sentence_values.append(sentence)
    for line in test_file:
        lowercase_line = line.lower()
        for i in symbols:
            if(i in lowercase_line):
                lowercase_line = lowercase_line.replace(i,"")
        test_sentences.append(lowercase_line)
    vectorized_info = vectorizer(sentence_values,test_sentences)
    if classifier_type == "lr":
        train_and_testLR(vectorized_info[0],vectorized_info[1],prediction_values,classifier_type,version)
    if classifier_type == "nb":
        train_and_testNB(vectorized_info[0],vectorized_info[1],prediction_values,classifier_type, version)

    

#unnormalize() is meant to just obtain the unnormalized information from the data given, which is to be used in training and test files.
def unnormalize(filename,classifier_type,version):
    prediction_values = []
    sentence_values = []
    test_sentences = []
    file_info = open("testingData.txt", "r")
    test_file = open(filename,"r")
    for line in file_info:
        line_list = line.split()
        if line_list[len(line_list) - 1] == '0':
            prediction_values.append('0')
        if line_list[len(line_list) - 1] == '1':
            prediction_values.append('1')
        del line_list[len(line_list) - 1]
        sentence = assemble(line_list)
        sentence_values.append(sentence)
    for line in test_file:
        test_sentences.append(line)
    vectorized_info = vectorizer(sentence_values,test_sentences)
    if classifier_type == "lr":
        train_and_testLR(vectorized_info[0],vectorized_info[1],prediction_values,classifier_type,version)
    if classifier_type == "nb":
        train_and_testNB(vectorized_info[0],vectorized_info[1],prediction_values,classifier_type,version)
    


#enabling the program to be run from the command line
if __name__ == "__main__":
    classifier = sys.argv[1]
    version_type = sys.argv[2]
    text_file = sys.argv[3]
    if version_type == "u":
        unnormalize(text_file,classifier,version_type)
    elif version_type == "n":
        normalize(text_file,classifier,version_type)
    
    
    


# In[ ]:





# In[ ]:





# In[ ]:




