
#!/usr/bin/env python
# coding: utf-8

# In[20]:


import numpy as np
import sys


# In[23]:


#the min_edit_distance() function is used to find the minimum edit distance between two given words
def min_edit_distance(source_word,target_word):
    n = len(source_word)
    m = len(target_word)
    insert_cost = 1
    delete_cost = 1
    #creating a distance matrix
    matrix = np.zeros((n+1,m+1),dtype = int)
    #for every row in the matrix, in the first column
    for i in range(1,n+1):
        matrix[i,0] = matrix[i-1,0] + delete_cost
    for j in range(1,m+1):
        matrix[0,j] = matrix[0,j-1] + insert_cost
    #Recurrence relation
    for i in range(1,n+1):
        for j in range(1,m+1):
            a = matrix[i-1,j]+delete_cost
            b = matrix[i-1,j-1]+substitution_cost(source_word[i-1],target_word[j-1])
            c = matrix[i,j-1]+insert_cost
            matrix[i,j] = min(a,b,c)
    print("Minimum edit distance between "+source_word+" and "+target_word+" is "+str(matrix[n,m]))
    
    return matrix[n,m]

def substitution_cost(source,target):
    if source != target:
        return 2
    if source == target:
        return 0
    
if __name__=="__main__":
    word1 = sys.argv[1]
    word2 = sys.argv[2]
    min_edit_distance(word1,word2)


# In[ ]:




