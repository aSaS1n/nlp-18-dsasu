#!/usr/bin/env python
# coding: utf-8

# In[47]:


datasets = ["amazon_cells_labelled.txt","imdb_labelled.txt","yelp_labelled.txt"]

#The derivesets() function below is meant to derive training and testing datasets from which we can calculate
#the accuracy of the naive bayes classifier
def derivesets(files):
    count = 0
    for i in files:
        test = open("testingData.txt","w")
        train = open("trainingData.txt","w")
        currentfile = open(i,"r")
        for line in currentfile:
            if count == 0:
                #write to the training dataset file
                train.write(line)
                count = 1
            else:
                #write to the testing dataset file
                test.write(line)
                count = 0
        currentfile.close()
        train.close()
        test.close()
    return

derivesets(datasets)


# In[48]:


import math
import sys
#The assumption being implemented here is that each sentence represents a document.

#The construct() function below is meant to construct sentences into strings, after they have been split into lists
def construct(sentence):
    new_sentence = ""
    del sentence[len(sentence)-1]
    for i in range(len(sentence)):
        new_sentence = sentence[i]+" "
    return new_sentence

#The obtain_accuracy() function below is meant to calculate the accuracy of the naive bayes classifier
def obtain_accuracy(file,logprior,vocab_all,likelihood0,likelihood1):
    total_num_sentences = 0
    total_num_corrects = 0
    testing_data = open(file,"r")
    for line in testing_data:
        total_num_sentences += 1
        tag = int(line[-2])
        sentence_without_tag = line[:-2]
        prediction = test_naive_bayes(sentence_without_tag,logprior,[0,1],vocab_all,likelihood0,likelihood1)
        if tag == prediction:
            total_num_corrects += 1
    print("Total number of sentences: ",total_num_sentences)
    print("Correctly predicted: ",total_num_corrects)
    accuracy = (total_num_corrects/float(total_num_sentences)) * 100
    print("Percentage of accuracy: ",accuracy,"%")
    return


#count_words_all() function is used to find out the number of times a given word appears in the textfiles.
def count_words_all(line_provided,dict_for_all):
    for i in line_provided:
        if i in dict_for_all:
            dict_for_all[i] += 1
        else:
            dict_for_all[i] = 1
    return dict_for_all

#count_word_class0() function is used to find out the number of times a given word appears in class 0.
def count_words_class0(line_provided,dict_for_class0):
    for i in line_provided:
        if i in dict_for_class0:
            dict_for_class0[i] += 1
        else:
            dict_for_class0[i] = 1
    return dict_for_class0

#count_words_class1() function is used to find out the number of times a given word appears in class 1.
def count_words_class1(line_provided,dict_for_class1):
    for i in line_provided:
        if i in dict_for_class1:
            dict_for_class1[i] += 1
        else:
            dict_for_class1[i] = 1
    return dict_for_class1
            
#denominator_for_likelihood0() is used to calculate the denominator for the likelihood estimate in class 0.            
def denominator_for_likelihood0(dictionary,count_for_class0):
    denom = 0
    keys = list(dictionary.keys())
    for key in keys:
        if key in count_for_class0:
            denom = denom + count_for_class0[key] + 1
        else:
            denom += 1
    return denom

#denominator_for_likelihood1() is used to calculate the denominator for the likelihood estimate in class 1.
def denominator_for_likelihood1(dictionary,count_for_class1):
    denom = 0
    keys = list(dictionary.keys())
    for key in keys:
        if key in count_for_class1:
            denom = denom + count_for_class1[key] + 1
        else:
            denom += 1
    return denom
    

#likelihood_for_class0() is used for obtaining the likelihood of the words that are present in class 0.
def likelihood_for_class0(dictionary_of_all_words,dict_likelihood_given0,count_for_class0):
    keys = list(dictionary_of_all_words.keys())
    denominator = denominator_for_likelihood0(dictionary_of_all_words,count_for_class0)
    for key in keys:
        if key in count_for_class0:
            numerator = count_for_class0[key] + 1
            dict_likelihood_given0[key] = math.log(numerator/float(denominator))
        else:
            numerator = 1
            dict_likelihood_given0[key] = math.log(numerator/float(denominator))
    return dict_likelihood_given0
    
#likelihood_for_class1() is used for obtaining the likelihood of the words that are present in class 1.    
def likelihood_for_class1(dictionary_of_all_words,dict_likelihood_given1,count_for_class1):
    keys = list(dictionary_of_all_words.keys())
    denominator = denominator_for_likelihood1(dictionary_of_all_words,count_for_class1)
    for key in keys:
        if key in count_for_class1:
            numerator = count_for_class1[key] + 1
            dict_likelihood_given1[key] = math.log(numerator/float(denominator))
        else:
            numerator = 1
            dict_likelihood_given1[key] = math.log(numerator/float(denominator))
            
    return dict_likelihood_given1
   
            

            
#The train_naive_bayes() function is meant to train develop and train 
#a sentiment classifier using naive bayes.
def train_naive_bayes(files_to_be_trained_on):
    count_for_class1 = {}
    count_for_class0 = {}
    vocab_all = {}
    logprior = []
    loglikelihood = []
    likelihood_of_words_given0 = {}
    likelihood_of_words_given1 = {}
    #num_of_classes is used to keep track of the number of classes 
    num_of_classes = 2
    
    #num_of_documents is used to keep track of the number of sentences in all the files to be read
    num_of_documents = 0
    
    #num_of_documents_for_class1 is used to keep track of the number of sentences belonging to class 1
    num_of_documents_for_class1 = 0
    
    #num_of_documents_for_class0 is used to keep track of the number of sentences belonging to class 0
    num_of_documents_for_class0 = 0
    
    #zero_class is meant to be the bag of words for the 
    #sentences with the class value of zero.
    zero_class = []
    
    #one_class is meant to be the bag of words for the 
    #sentences with the class value of one
    one_class = []
    
    #for each file to be trained on:
    # - open the file, read a line in the file, split the line, find out the class the line belongs to and 
    # place the line in the class it belongs.
    for i in files_to_be_trained_on:
        current_file = open(i,"r")
        for line in current_file:
            #increment the value of num_of_documents in order to find out the total number of documents (i.e sentences)
            num_of_documents += 1
            #split the line to find out whether it contains a zero or a one class
            line_list = line.split()
            
            
            #if the line contains a '1' class, append the line to one_class[] to create a bag of words
            #for the sentences with class '1'
            if '1' in line_list:
                del line_list[len(line_list) - 1]
                one_class.append(line_list)
                num_of_documents_for_class1 += 1
                count_for_class1 = count_words_class1(line_list,count_for_class1)
            
            #if the line contains a '0' class, append the line to zero_class[] to create a bag of words for the sentences
            #with class '0'
            if '0' in line_list:
                del line_list[len(line_list) - 1]
                zero_class.append(line_list)
                num_of_documents_for_class0 += 1
                count_for_class0 = count_words_class0(line_list,count_for_class0)
            
            vocab_all = count_words_all(line_list,vocab_all)
        
              
    
    
    #obtaining the prior probabilities for class 0
    logprior.append(math.log(num_of_documents_for_class0/float(num_of_documents)))
    #obtaining the prior probabilities for class 1
    logprior.append(math.log(num_of_documents_for_class1/float(num_of_documents)))
    
    likelihood_of_words_given0 = likelihood_for_class0(vocab_all,likelihood_of_words_given0,count_for_class0)
    loglikelihood.append(likelihood_of_words_given0)
    likelihood_of_words_given1 = likelihood_for_class1(vocab_all,likelihood_of_words_given1,count_for_class1)
    loglikelihood.append(likelihood_of_words_given1)
    
    return logprior, vocab_all,likelihood_of_words_given0, likelihood_of_words_given1

#the test_naive_bayes() function is used for obtaining a prediction from the classifier for any sentence that it is provided
def test_naive_bayes(testdocument, logpriors, classes, vocabulary,likelihood_of_words_given0,likelihood_of_words_given1):
    probabilities = []
    test = testdocument.split()
    for i in range(len(classes)):
        probabilities.append(logpriors[i])
        for word in test:
            if word in list(vocabulary.keys()):
                if i == 0:
                    probabilities[i] = probabilities[i] + likelihood_of_words_given0[word]
                if i == 1:
                    probabilities[i] = probabilities[i] + likelihood_of_words_given1[word]
                    
    if probabilities[0] > probabilities[1]:
        return 0
    elif probabilities[1] > probabilities[0]:
        return 1

#processfile() opens the file provided in the command line and it reads the lines of the file
#and provides the test_naive_bayes function with each sentence in the file provided for prediction
def processfile(filename,logprior,vocab_all,likelihood0,likelihood1):
    file = open(filename,"r")
    wr_file = open("results_file.txt","w")
    for line in file:
        prediction = test_naive_bayes(line,logprior,[0,1],vocab_all,likelihood0,likelihood1)
        wr_file.write(str(prediction)+"\n")
    wr_file.close()
    return 




if __name__ == "__main__":
    logpriors,vocabulary,likelihood_given0, likelihood_given1 = train_naive_bayes(["amazon_cells_labelled.txt","imdb_labelled.txt","yelp_labelled.txt"])
    #obtaining the name of the file provided with the test sentences
    filename = sys.argv[1]
    #call processfile() on the file name provided.
    processfile(filename,logpriors,vocabulary,likelihood_given0,likelihood_given1)
    
    #uncomment the obtain_accuracy() function below only when you want to check the accuracy of the classifier
    #obtain_accuracy("testingData.txt",logpriors,vocabulary,likelihood_given0,likelihood_given1)
    



# In[ ]:




