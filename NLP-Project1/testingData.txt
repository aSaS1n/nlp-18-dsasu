Crust is not good.	0
Stopped by during the late May bank holiday off Rick Steve recommendation and loved it.	1
Now I am getting angry and I want my damn pho.	0
The potatoes were like rubber and you could tell they had been made up ahead of time being kept under a warmer.	0
A great touch.	1
Would not go back.	0
I tried the Cape Cod ravoli, chicken,with cranberry...mmmm!	1
I was shocked because no signs indicate cash only.	0
Waitress was a little slow in service.	0
did not like at all.	0
The food, amazing.	1
I could care less... The interior is just beautiful.	1
That's right....the red velvet cake.....ohhh this stuff is so good.	1
This hole in the wall has great Mexican street tacos, and friendly staff.	1
The worst was the salmon sashimi.	0
This was like the final blow!	0
seems like a good quick place to grab a bite of some familiar pub food, but do yourself a favor and look elsewhere.	0
The only redeeming quality of the restaurant was that it was very inexpensive.	1
Poor service, the waiter made me feel like I was stupid every time he came to the table.	0
Service sucks.	0
There is not a deal good enough that would drag me into that establishment again.	0
On a positive note, our server was very attentive and provided great service.	1
The only thing I did like was the prime rib and dessert section.	1
The burger is good beef, cooked just right.	1
My side Greek salad with the Greek dressing was so tasty, and the pita and hummus was very refreshing.	1
He came running after us when he realized my husband had left his sunglasses on the table.	1
They have horrible attitudes towards customers, and talk down to each one when customers don't enjoy their food.	0
Loved it...friendly servers, great food, wonderful and imaginative menu.	1
Not much seafood and like 5 strings of pasta at the bottom.	0
The ripped banana was not only ripped, but petrified and tasteless.	0
This place receives stars for their APPETIZERS!!!	1
We'd definitely go back here again.	1
Great food and service, huge portions and they give a military discount.	1
Update.....went back for a second time and it was still just as amazing	1
A great way to finish a great.	1
- Really, really good rice, all the time.	1
It took over 30 min to get their milkshake, which was nothing more than chocolate milk.	0
The scallop dish is quite appalling for value as well.	0
The sweet potato fries were very good and seasoned well.	1
There is so much good food in Vegas that I feel cheated for wasting an eating opportunity by going to Rice and Company.	0
walked in and the place smelled like an old grease trap and only 2 others there eating.	0
This place has it!	1
I love the Pho and the spring rolls oh so yummy you have to try.	1
All I have to say is the food was amazing!!!	1
Everything was fresh and delicious!	1
It's like a really sexy party in your mouth, where you're outrageously flirting with the hottest person at the party.	1
Best breakfast buffet!!!	1
We'll never go again.	0
Food arrived quickly!	1
On the up side, their cafe serves really good food.	1
The only good thing was our waiter, he was very helpful and kept the bloddy mary's coming.	1
I LOVED their mussels cooked in this wine reduction, the duck was tender, and their potato dishes were delicious.	1
So we went to Tigerlilly and had a fantastic afternoon!	1
The ambience is wonderful and there is music playing.	1
Sooooo good!!	1
At least 40min passed in between us ordering and the food arriving, and it wasn't that busy.	0
Nice, spicy and tender.	1
Check it out.	1
I've had better atmosphere.	0
Although I very much liked the look and sound of this place, the actual experience was a bit disappointing.	0
Worst service to boot, but that is the least of their worries.	0
The guys all had steaks, and our steak loving son who has had steak at the best and worst places said it was the best steak he's ever eaten.	1
Host staff were, for lack of a better word, BITCHES!	0
Phenomenal food, service and ambiance.	1
Definitely worth venturing off the strip for the pork belly, will return next time I'm in Vegas.	1
Penne vodka excellent!	1
The management is rude.	0
Great Subway, in fact it's so good when you come here every other Subway will not meet your expectations.	1
This is one of the best bars with food in Vegas.	1
My drink was never empty and he made some really great menu suggestions.	1
The waiter wasn't helpful or friendly and rarely checked on us.	0
And the red curry had so much bamboo shoots and wasn't very tasty to me.	0
The bathrooms are clean and the place itself is well decorated.	1
The service was a little slow , considering that were served by 3 people servers so the food was coming in a slow pace.	0
We watched our waiter pay a lot more attention to other tables and ignore us.	0
This is a great restaurant at the Mandalay Bay.	1
Crostini that came with the salad was stale.	0
the staff is friendly and the joint is always clean.	1
I ordered the Voodoo pasta and it was the first time I'd had really excellent pasta since going gluten free several years ago.	1
Unfortunately, we must have hit the bakery on leftover day because everything we ordered was STALE.	0
I was seated immediately.	1
Avoid at all cost!	0
DELICIOUS!!	1
So don't go there if you are looking for good food...	0
Bacon is hella salty.	1
This really is how Vegas fine dining used to be, right down to the menus handed to the ladies that have no prices listed.	1
Lordy, the Khao Soi is a dish that is not to be missed for curry lovers!	1
Perhaps I caught them on an off night judging by the other reviews, but I'm not inspired to go back.	0
The atmosphere is modern and hip, while maintaining a touch of coziness.	1
We literally sat there for 20 minutes with no one asking to take our order.	0
I also decided not to send it back because our waitress looked like she was on the verge of having a heart attack.	0
It was probably dirt.	0
I ordered the Lemon raspberry ice cocktail which was also incredible.	1
Interesting decor.	1
Also were served hot bread and butter, and home made potato chips with bacon bits on top....very original and very good.	1
Both of the egg rolls were fantastic.	1
I had a salad with the wings, and some ice cream for dessert and left feeling quite satisfied.	1
The best place to go for a tasty bowl of Pho!	1
I've never been more insulted or felt disrespected.	0
It is worth the drive.	1
Food was great and so was the serivce!	1
Great brunch spot.	1
Very good lunch spot.	1
The WORST EXPERIENCE EVER.	0
The sides are delish - mixed mushrooms, yukon gold puree, white corn - beateous.	1
For about 10 minutes, we we're waiting for her salad when we realized that it wasn't coming any time soon.	0
Won't go back.	0
Waitress was good though!	1
The Jamaican mojitos are delicious.	1
- the food is rich so order accordingly.	1
The service was a bit lacking.	0
Hopefully this bodes for them going out of business and someone who can cook can come in.	0
I loved the bacon wrapped dates.	1
The folks at Otto always make us feel so welcome and special.	1
This is the place where I first had pho and it was amazing!!	1
If the food isn't bad enough for you, then enjoy dealing with the world's worst/annoying drunk people.	0
Ordered a double cheeseburger & got a single patty that was falling apart (picture uploaded) Yeah, still sucks.	0
If it were possible to give them zero stars, they'd have it.	0
I'd say that would be the hardest decision... Honestly, all of M's dishes taste how they are supposed to taste (amazing).	1
Everyone is very attentive, providing excellent customer service.	1
Now this dish was quite flavourful.	1
(It wasn't busy either) Also, the building was FREEZING cold.	0
-Drinks took close to 30 minutes to come out at one point.	0
Much better than the other AYCE sushi place I went to in Vegas.	1
Based on the sub-par service I received and no effort to show their gratitude for my business I won't be going back.	0
There is nothing privileged about working/eating there.	0
Overall, I don't think that I would take my parents to this place again because they made most of the similar complaints that I silently felt too.	0
We had 7 at our table and the service was pretty fast.	1
I as well would've given godfathers zero stars if possible.	0
very tough and very short on flavor!	0
I have been in more than a few bars in Vegas, and do not ever recall being charged for tap water.	0
Good service, very clean, and inexpensive, to boot!	1
Plus, it's only 8 bucks.	1
Thus far, have only visited twice and the food was absolutely delicious each time.	1
For a self proclaimed coffee cafe, I was wildly disappointed.	0
You cant go wrong with any of the food here.	1
Stopped by this place while in Madison for the Ironman, very friendly, kind staff.	1
I've had better, not only from dedicated boba tea spots, but even from Jenni Pho.	0
The goat taco didn't skimp on the meat and wow what FLAVOR!	1
I had the mac salad and it was pretty bland so I will not be getting that again.	0
Service stinks here!	0
This place is not quality sushi, it is not a quality restaurant.	0
Great Pizza and Salads!	1
We waited an hour for what was a breakfast I could have done 100 times better at home.	0
I hate to disagree with my fellow Yelpers, but my husband and I were so disappointed with this place.	0
Just don't know why they were so slow.	0
I live in the neighborhood so I am disappointed I won't be back here, because it is a convenient location.	0
You get incredibly fresh fish, prepared with care.	1
I love the fact that everything on their menu is worth it.	1
The food was excellent and service was very good.	1
Please stay away from the shrimp stir fried noodles.	0
Food was really boring.	0
This greedy corporation will NEVER see another dime from me!	0
As much as I'd like to go back, I can't get passed the atrocious service and will never return.	0
I did not expect this to be so good!	1
She ordered a toasted English muffin that came out untoasted.	0
Never going back.	0
The bus boy on the other hand was so rude.	0
Back to good BBQ, lighter fare, reasonable pricing and tell the public they are back to the old ways.	1
All the bread is made in-house!	1
Also, the fries are without a doubt the worst fries I've ever had.	0
A couple of months later, I returned and had an amazing meal.	1
The black eyed peas and sweet potatoes... UNREAL!	1
They could serve it with just the vinaigrette and it may make for a better overall dish, but it was still very good.	1
When my mom and I got home she immediately got sick and she only had a few bites of salad.	0
Both of them were truly unbelievably good, and I am so glad we went back.	1
Everything was gross.	0
Great service and food.	1
The burger... I got the "Gold Standard" a $17 burger and was kind of disappointed.	0
There is nothing authentic about this place.	0
Of all the dishes, the salmon was the best, but all were great.	1
It's worth driving up from Tucson!	1
Pretty good beer selection too.	1
Classy/warm atmosphere, fun and fresh appetizers, succulent steaks (Baseball steak!!!!!	1
I have eaten here multiple times, and each time the food was delicious.	1
He was terrible!	0
It shouldn't take 30 min for pancakes and eggs.	0
On the good side, the staff was genuinely pleasant and enthusiastic - a real treat.	1
As always the evening was wonderful and the food delicious!	1
(The bathroom is just next door and very nice.)	1
This is an Outstanding little restaurant with some of the Best Food I have ever tasted.	1
Definitely a turn off for me & i doubt I'll be back unless someone else is buying.	0
I find wasting food to be despicable, but this just wasn't food.	0
Would come back again if I had a sushi craving while in Vegas.	1
He deserves 5 stars.	1
They dropped more than the ball.	0
They will customize your order any way you'd like, my usual is Eggplant with Green Bean stir fry, love it!	1
Best tacos in town by far!!	1
In an interesting part of town, this place is amazing.	1
The staff are now not as friendly, the wait times for being served are horrible, no one even says hi for the first 10 minutes.	0
They have great dinners.	1
The food was terrible.	0
I don't recommend unless your car breaks down in front of it and you are starving.	0
This place deserves one star and 90% has to do with the food.	0
Def coming back to bowl next time	1
I will continue to come here on ladies night andddd date night ... highly recommend this place to anyone who is in the area (;	1
We walked away stuffed and happy about our first Vegas buffet experience.	1
To summarize... the food was incredible, nay, transcendant... but nothing brings me joy quite like the memory of the pneumatic condiment dispenser.	1
Kids pizza is always a hit too with lots of great side dish options for the kiddos!	1
Cooked to perfection and the service was impeccable.	1
Overall, I was very disappointed with the quality of food at Bouchon.	0
Great place to eat, reminds me of the little mom and pop shops in the San Francisco Bay Area.	1
Left very frustrated.	0
Food was really good and I got full petty fast.	1
TOTAL WASTE OF TIME.	0
Come hungry, leave happy and stuffed!	1
I can assure you that you won't be disappointed.	1
Gave up trying to eat any of the crust (teeth still sore).	0
I really enjoyed eating here.	1
Our server was very nice, and even though he looked a little overwhelmed with all of our needs, he stayed professional and friendly until the end.	1
On the ground, right next to our table was a large, smeared, been-stepped-in-and-tracked-everywhere pile of green bird poop.	0
We've tried to like this place but after 10+ times I think we're done with them.	0
No complaints!	1
Waiter was a jerk.	0
These are the nicest restaurant owners I've ever come across.	1
We loved the biscuits!!!	1
Ordered an appetizer and took 40 minutes and then the pizza another 10 minutes.	0
It was a huge awkward 1.5lb piece of cow that was 3/4ths gristle and fat.	0
I like Steiners because it's dark and it feels like a bar.	1
If you're not familiar, check it out.	1
I'd love to go back.	1
Nothing special.	0
Not to mention the combination of pears, almonds and bacon is a big winner!	1
Sauce was tasteless.	0
My ribeye steak was cooked perfectly and had great mesquite flavor.	1
Food was so gooodd.	1
I was so insulted.	0
The chicken wings contained the driest chicken meat I have ever eaten.	0
Nargile - I think you are great.	1
We loved the place.	1
The vanilla ice cream was creamy and smooth while the profiterole (choux) pastry was fresh enough.	1
The manager was the worst.	0
The food was outstanding and the prices were very reasonable.	1
This is was due to the fact that it took 20 minutes to be acknowledged, then another 35 minutes to get our food...and they kept forgetting things.	0
This was my first and only Vegas buffet and it did not disappoint.	1
The one down note is the ventilation could use some upgrading.	0
Don't waste your time here.	0
Third, the cheese on my friend's burger was cold.	0
The steaks are all well trimmed and also perfectly cooked.	1
I LOVED it!	1
This place is a jewel in Las Vegas, and exactly what I've been hoping to find in nearly ten years living here.	1
The selection of food was not the best.	0
This isn't a small family restaurant, this is a fine dining establishment.	1
I dont think I will be back for a very long time.	0
How awesome is that.	1
The menu had so much good stuff on it i could not decide!	1
CONCLUSION: Very filling meals.	1
And then tragedy struck.	0
This was my first crawfish experience, and it was delicious!	1
Waitress was sweet and funny.	1
I'd rather eat airline food, seriously.	0
The ambiance was incredible.	1
I would not recommend this place.	0
My gyro was basically lettuce only.	0
Thoroughly disappointed!	0
Give it a try, you will be happy you did.	1
Reasonably priced also!	1
The food is very good for your typical bar food.	1
At first glance it is a lovely bakery cafe - nice ambiance, clean, friendly staff.	1
Point your finger at any item on the menu, order it and you won't be disappointed.	1
If you haven't gone here GO NOW!	1
first time there and might just be the last.	0
Similarly, the delivery man did not say a word of apology when our food was 45 minutes late.	0
Be sure to order dessert, even if you need to pack it to-go - the tiramisu and cannoli are both to die for.	1
The bartender was also nice.	1
This place is two thumbs up....way up.	1
If you love authentic Mexican food and want a whole bunch of interesting, yet delicious meats to choose from, you need to try this place.	1
An excellent new restaurant by an experienced Frenchman.	1
Great steak, great sides, great wine, amazing desserts.	1
The steak and the shrimp are in my opinion the best entrees at GC.	1
We waited for thirty minutes to be seated (although there were 8 vacant tables and we were the only folks waiting).	0
I won't try going back there even if it's empty.	0
Just spicy enough.. Perfect actually.	1
not even a "hello, we will be right with you."	0
My boyfriend and I came here for the first time on a recent trip to Vegas and could not have been more pleased with the quality of food and service.	1
Nice ambiance.	1
I guess maybe we went on an off night but it was disgraceful.	0
I know this is not like the other restaurants at all, something is very off here!	0
I think this restaurant suffers from not trying hard enough.	0
I *heart* this place.	1
After two I felt disgusting.	0
I believe that this place is a great stop for those with a huge belly and hankering for sushi.	1
I will never go back to this place and will never ever recommended this place to anyone!	0
Food was delicious!	1
I consider this theft.	0
We recently witnessed her poor quality of management towards other guests as well.	0
He also came back to check on us regularly, excellent service.	1
The pizza tasted old, super chewy in not a good way.	0
Service was good and the company was better!	1
As for the service: I'm a fan, because it's quick and you're being served by some nice folks.	1
Over rated.	0
Their steaks are 100% recommended!	1
Great food and great service in a clean and friendly setting.	1
I hate those things as much as cheap quality black olives.	0
The kids play area is NASTY!	0
The waitress was friendly and happy to accomodate for vegan/veggie options.	1
It was extremely "crumby" and pretty tasteless.	0
The croutons also taste homemade which is an extra plus.	1
It'll be a regular stop on my trips to Phoenix!	1
Not good for the money.	0
We got sitting fairly fast, but, ended up waiting 40 minutes just to place our order, another 30 minutes before the food arrived.	0
Good value, great food, great service.	1
The food is good.	1
I just wanted to leave.	0
I will not be eating there again.	0
I checked out this place a couple years ago and was not impressed.	0
Sorry, I will not be getting food from here anytime soon :(	0
The cow tongue and cheek tacos are amazing.	1
Despite how hard I rate businesses, its actually rare for me to give a 1 star.	0
I will not return.	0
Very disappointing!!!	0
a drive thru means you do not want to wait around for half an hour for your food, but somehow when we end up going here they make us wait and wait.	0
Ambience is perfect.	1
Any grandmother can make a roasted chicken better than this one.	0
The staff is always super friendly and helpful, which is especially cool when you bring two small boys and a baby!	1
The roast beef sandwich tasted really good!	1
High-quality chicken on the chicken Caesar salad.	1
We were promptly greeted and seated.	1
I was proven dead wrong by this sushi bar, not only because the quality is great, but the service is fast and the food, impeccable.	1
This is a good joint.	1
I'm not eating here!	0
Maybe if they weren't cold they would have been somewhat edible.	0
Very bad Experience!	0
Food was average at best.	0
We won't be going back anytime soon!	0
Great place to relax and have an awesome burger and beer.	1
Not much flavor to them, and very poorly constructed.	0
The fried rice was dry as well.	0
That just SCREAMS "LEGIT" in my book...somethat's also pretty rare here in Vegas.	1
The atmosphere was great with a lovely duo of violinists playing songs we requested.	1
Very convenient, since we were staying at the MGM!	1
Both great!	1
The sweet potato tots were good but the onion rings were perfection or as close as I have had.	1
And the chef was generous with his time (even came around twice so we can take pictures with him).	1
Google mediocre and I imagine Smashburger will pop up.	0
I promise they won't disappoint.	1
What a great double cheeseburger!	1
A fantastic neighborhood gem !!!	1
The plantains were the worst I've ever tasted.	0
Service was slow and not attentive.	0
Your staff spends more time talking to themselves than me.	0
Very good food, great atmosphere.1	1
Total brunch fail.	0
The decor is nice, and the piano music soundtrack is pleasant.	1
Good food , good service .	1
I probably won't be back, to be honest.	0
The sergeant pepper beef sandwich with auju sauce is an excellent sandwich as well.	1
Went for lunch - service was slow.	0
I was mortified.	0
Anyways, The food was definitely not filling at all, and for the price you pay you should expect more.	0
I wasn't really impressed with Strip Steak.	0
Our server was very nice and attentive as were the other serving staff.	1
I work in the hospitality industry in Paradise Valley and have refrained from recommending Cibo any longer.	0
Would not recommend to others.	0
I mean really, how do you get so famous for your fish and chips when it's so terrible!?!	0
Not my thing.	0
If you are reading this please don't go there.	0
Only Pros : Large seating area/ Nice bar area/ Great simple drink menu/ The BEST brick oven pizza with homemade dough!	1
Tonight I had the Elk Filet special...and it sucked.	0
We ordered some old classics and some new dishes after going there a few times and were sorely disappointed with everything.	0
The chicken was deliciously seasoned and had the perfect fry on the outside and moist chicken on the inside.	1
Special thanks to Dylan T. for the recommendation on what to order :) All yummy for my tummy.	1
Great food and awesome service!	1
A FLY was in my apple juice.. A FLY!!!!!!!!	0
As for the service, I thought it was good.	1
Ryan's Bar is definitely one Edinburgh establishment I won't be revisiting.	0
Overall, I like there food and the service.	1
Probably never coming back, and wouldn't recommend it.	0
Try them in the airport to experience some tasty food and speedy, friendly service.	1
Never had anything to complain about here.	1
It was way over fried.	0
When I opened the sandwich, I was impressed, but not in a good way.	0
There was a warm feeling with the service and I felt like their guest for a special treat.	1
I always order from the vegetarian menu during dinner, which has a wide array of options to choose from.	1
Wonderful lil tapas and the ambience made me feel all warm and fuzzy inside.	1
The wontons were thin, not thick and chewy, almost melt in your mouth.	1
We were sat right on time and our server from the get go was FANTASTIC!	1
When I'm on this side of town, this will definitely be a spot I'll hit up again!	1
This is a GREAT place to eat!	1
The service was poor and thats being nice.	0
The place was not clean and the food oh so stale!	0
But the service was beyond bad.	0
Tasted like dirt.	0
The block was amazing.	1
* Both the Hot & Sour & the Egg Flower Soups were absolutely 5 Stars!	1
Great time - family dinner on a Sunday night.	1
What did bother me, was the slow service.	0
Their frozen margaritas are WAY too sugary for my taste.	0
So in a nutshell: 1) The restaraunt smells like a combination of a dirty fish market and a sewer.	0
Unfortunately, it was not good.	0
Join the club and get awesome offers via email.	1
Bland and flavorless is a good way of describing the barely tepid meat.	0
The nachos are a MUST HAVE!	1
I don't have very many words to say about this place, but it does everything pretty well.	1
Great atmosphere, friendly and fast service.	1
Once your food arrives it's meh.	0
The classic Maine Lobster Roll was fantastic.	1
So good I am going to have to review this place twice - once hereas a tribute to the place and once as a tribute to an event held here last night.	1
This place is great!!!!!!!!!!!!!!	1
Once you get inside you'll be impressed with the place.	1
And service was super friendly.	1
This place was such a nice surprise!	1
I had high hopes for this place since the burgers are cooked over a charcoal grill, but unfortunately the taste fell flat, way flat.	0
Not a single employee came out to see if we were OK or even needed a water refill once they finally served us our food.	0
The first time I ever came here I had an amazing experience, I still tell people how awesome the duck was.	1
The service was terrible though.	0
It was packed!!	0
I can say that the desserts were yummy.	1
The seasonal fruit was fresh white peach puree.	1
This place should honestly be blown up.	0
Do not waste your money here!	0
The crêpe was delicate and thin and moist.	1
Won't ever go here again.	0
For that price I can think of a few place I would have much rather gone.	0
I do love sushi, but I found Kabuki to be over-priced, over-hip and under-services.	0
Very poor service.	0
Best service and food ever, Maria our server was so good and friendly she made our day.	1
I paid the bill but did not tip because I felt the server did a terrible job.	0
I have never had such bland food which surprised me considering the article we read focused so much on their spices and flavor.	0
I recently tried Caballero's and I have been back every week since!	1
The food came out at a good pace.	1
I won't be back.	0
This place deserves no stars.	0
In fact I'm going to round up to 4 stars, just because she was so awesome.	1
Bad day or not, I have a very low tolerance for rude customer service people, it is your job to be nice and polite, wash dishes otherwise!!	0
I probably would not go here again.	0
The price is reasonable and the service is great.	1
My fella got the huevos rancheros and they didn't look too appealing.	0
Some may say this buffet is pricey but I think you get what you pay for and this place you are getting quite a lot!	1
Worst food/service I've had in a while.	0
Talk about great customer service of course we will be back.	1
I love their fries and their beans.	1
They have a plethora of salads and sandwiches, and everything I've tried gets my seal of approval.	1
For sushi on the Strip, this is the place to go.	1
The feel of the dining room was more college cooking course than high class dining and the service was slow at best.	0
this is the worst sushi i have ever eat besides Costco's.	0
My boyfriend and i sat at the bar and had a completely delightful experience.	1
There was hardly any meat.	0
Go To Place for Gyros.	1
Now the burgers aren't as good, the pizza which used to be amazing is doughy and flavorless.	0
The service was terrible, food was mediocre.	0
I ordered Albondigas soup - which was just warm - and tasted like tomato soup with frozen meatballs.	0
I had about two bites and refused to eat anymore.	0
After 20 minutes wait, I got a table.	0
No allergy warnings on the menu, and the waitress had absolutely no clue as to which meals did or did not contain peanuts.	0
Their rotating beers on tap is also a highlight of this place.	1
Worst Thai ever.	0
I want to first say our server was great and we had perfect service.	1
I had strawberry tea, which was good.	1
Overall, a great experience.	1
Their regular toasted bread was equally satisfying with the occasional pats of butter... Mmmm...!	1
And the drinks are WEAK, people!	0
Also, I feel like the chips are bought, not made in house.	0
The chips and sals a here is amazing!!!!!!!!!!!!!!!!!!!	1
This is my new fav Vegas buffet spot.	1
Very, very sad.	0
How can you call yourself a steakhouse if you can't properly cook a steak, I don't understand!	0
The only thing I wasn't too crazy about was their guacamole as I don't like it puréed.	0
I got food poisoning here at the buffet.	0
What SHOULD have been a hilarious, yummy Christmas Eve dinner to remember was the biggest fail of the entire trip for us.	0
This place is disgusting!	0
The RI style calamari was a joke.	0
I could barely stomach the meal, but didn't complain because it was a business lunch.	0
It also took her forever to bring us the check when we asked for it.	0
Disappointing experience.	0
If you want to wait for mediocre food and downright terrible service, then this is the place for you.	0
We won't be going back.	0
This place lacked style!!	0
Don't bother coming here.	0
The building itself seems pretty neat; the bathroom is pretty trippy, but I wouldn't eat here again.	0
Probably not in a hurry to go back.	0
Not good by any stretch of the imagination.	0
The chipolte ranch dipping sause was tasteless, seemed thin and watered down with no heat.	0
I was VERY disappointed!!	0
Maybe it's just their Vegetarian fare, but I've been twice and I thought it was average at best.	0
The tables outside are also dirty a lot of the time and the workers are not always friendly and helpful with the menu.	0
Con: spotty service.	0
But then they came back cold.	0
The real disappointment was our waiter.	0
The only reason to eat here would be to fill up before a night of binge drinking just to get some carbs in your stomach.	0
If someone orders two tacos don't' you think it may be part of customer service to ask if it is combo or ala cart?	0
After all the rave reviews I couldn't wait to eat here......what a disappointment!	0
It's NOT hard to make a decent hamburger.	0
Hell no will I go back	0
I don't know what the big deal is about this place, but I won't be back "ya'all".	0
The ambiance isn't much better.	0
The food wasn't good.	0
What happened next was pretty....off putting.	0
Overpriced for what you are getting.	0
I kept looking at the time and it had soon become 35 minutes, yet still no food.	0
We started with the tuna sashimi which was brownish in color and obviously wasn't fresh.	0
It sure does beat the nachos at the movies but I would expect a little bit more coming from a restaurant.	0
The problem I have is that they charge $11.99 for a sandwich that is no bigger than a Subway sub (which offers better and more amount of vegetables).	0
It lacked flavor, seemed undercooked, and dry.	0
I would avoid this place if you are staying in the Mirage.	0
Spend your money and time some place else.	0
the presentation of the food was awful.	0
I think food should have flavor and texture and both were lacking.	0
Overall I was not impressed and would not go back.	0
Then, as if I hadn't wasted enough of my life there, they poured salt in the wound by drawing out the time it took to bring the check.	0
